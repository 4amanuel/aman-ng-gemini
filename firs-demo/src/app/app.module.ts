import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AlifeFileToBase64Module } from 'alife-file-to-base64';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { PostComponent } from './components/post/post.component';
import { PostNewComponent } from './components/post-new/post-new.component';
import { HomeWelcomeComponent } from './components/home-welcome/home-welcome.component';
import { PostCRUDService  } from './services/post-crud.service';

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostComponent,
    PostNewComponent,
    HomeWelcomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AlifeFileToBase64Module,
  ],
  providers: [ HttpClientModule, PostCRUDService ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  }
