export class Post {
  userId: number;
  id: number;
  title: string;
  body: string;
  photo?: any; // base64 image
  constructor(userid: number, id: number, title: string, body: string, photo: any = ''  ) {
    this.userId = userid;
    this.id = id;
    this.title = title;
    this.body = body;
    this.photo = photo;
  }
}
