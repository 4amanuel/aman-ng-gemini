import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from '../../models/post.model';
import { FormGroup, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post-new.component.css']
})
export class PostNewComponent implements OnInit {
  @Output() newPost = new EventEmitter<Post>();
  myNewPost: FormGroup;
  photo: string;


  constructor() { }

  ngOnInit() {
    this.myNewPost = new FormGroup({
       title: new FormControl('', [ Validators.required, Validators.minLength(2)]),
       body: new FormControl('', [ Validators.required, Validators.minLength(2)]),
       photo: new FormControl()
    });
  }

  onFileChanges(val: any) {
    const [{ base64 }] = val;
    this.photo = base64;
    console.log(val, base64);

  }

  onSubmit() {
    if (this.myNewPost.valid) {
      const {title, body} = this.myNewPost.value;
      // console.log('Form Submitted!', new Post(0, 1, title, body, this.photo));
      this.newPost.emit( new Post(0, 1, title, body, this.photo));
      this.myNewPost.reset();
    }
  }

}
