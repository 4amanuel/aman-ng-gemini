import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post.model';
import { PostCRUDService  } from '../../services/post-crud.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  posts: Array<Post>;
  deletedPost: Array<Post>;
  constructor(private postCrud: PostCRUDService) {
  }

  ngOnInit() {
    this.postCrud.getPosts().subscribe( newpost => {
      // pick the last 7 comments and sort them in early to late comments
      this.posts = newpost.reverse().slice(0, 7);
      // console.log(post);
    });
    this.deletedPost = [];
  }
  // Delete Posts
  removePosts(val: Post) {
    this.postCrud.delPosts(val).subscribe( pass => {
      this.posts = this.posts.filter( a => a.id !== val.id);
      this.deletedPost.push(val);
      console.log(this.deletedPost.map(a => a.id));

    });
  }

  // Add new Post
  addnewPosts(val: Post) {
    this.postCrud.addPosts(val).subscribe( pass => {
      val.id = this.posts[0].id + 1;
      this.posts = [val, ... this.posts];
      console.log('new ', val);

    });

  }

}
