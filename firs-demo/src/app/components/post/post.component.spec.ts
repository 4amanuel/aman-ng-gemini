import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Post } from '../../models/post.model';
import { PostComponent } from './post.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;
  let postbody: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    component.post = new Post(1, 1, 'title', 'any');
    postbody = fixture.debugElement.query(By.css('.card-text'));
    fixture.detectChanges();


  });


  it('should create, with @input', () => {
    expect(component).toBeTruthy();
  });

  it('should have body with text >any<, with @input', () => {
    expect(postbody.nativeElement.textContent).toBe('any');
  });

  it('should @Output Post ', () => {
    let delpost =  new Post(1, 2, 'title', 'any');
    component.removePost.subscribe( value => delpost = value );
    expect(delpost.id).toBe(2);
   });
});
