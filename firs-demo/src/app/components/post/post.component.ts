import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Post } from '../../models/post.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input() post: Post;
  @Output() removePost = new EventEmitter<Post>();
  constructor() { }

  // on Delete action , emit event
  onDelete(post: Post) {
    // console.log(post);
    this.removePost.emit( post);
  }

  ngOnInit() { }
}
