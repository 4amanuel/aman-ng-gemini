import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { PostCRUDService } from './post-crud.service';

describe('PostCRUDService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
     HttpClientModule
    ],

  }));

  it('should be created', () => {
    const service: PostCRUDService = TestBed.get(PostCRUDService);
    expect(service).toBeTruthy();
  });
});


  // new Post(1, 1, 'unt aut facere repellat', `quia et suscipit
      // suscipit recusandae consequuntur expedita et cum\nreprehenderit
      //  molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto`),
      // new Post(1, 2, 'unt aut facere repellat', `est rerum tempore vitae\nsequi sint nihil
      //  reprehenderit dolor b expedita et cum\nreprehenderit molestiae
      // ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto`),
      // new Post(1, 3, 'ea molestias quasi exercitationem',
      // `quia et suscipit\nsuscipit recusandae consequuntur expedita et
      // cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est
      //  autem sunt rem eveniet architecto`),
      // new Post(1, 1, 'eum et est occaecati',
      // `ullam et saepe reiciendis voluptatem adipisci\nsit
      //  amet autem assumenda provident rerum culpa\nquis hic commodi molestiae ut ut quas
      //  totam\nnostrum rerum est autem sunt rem eveniet architecto`)
