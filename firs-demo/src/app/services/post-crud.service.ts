import { Injectable, Pipe, Directive } from '@angular/core';
import { Post } from '../models/post.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const headers = {

    headers: new HttpHeaders({
      'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})

export class PostCRUDService {
  postURL = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) { }

  // CREATE Post
  addPosts(val: Post): Observable<any> {
  return this.http.post<Post>(this.postURL, val, headers);
  }
  // GET Post
  getPosts(): Observable<Array<Post>> {
   return this.http.get<Array<Post>>(this.postURL);
  }
  // DELETE Post
  delPosts(val: Post): Observable<any> {
    const url = `${this.postURL}/${val.id}`;
    return this.http.delete(url);
  }
}
