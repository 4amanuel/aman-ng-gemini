import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostListComponent } from './components/post-list/post-list.component';

import { HomeWelcomeComponent } from './components/home-welcome/home-welcome.component';

const routes: Routes = [

  { path: 'home', component: HomeWelcomeComponent },
  { path: 'show-post', component: PostListComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
