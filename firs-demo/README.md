# FirsDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## This Demo uses docker and docker-compose 
1) clone repo to your project directory 
``` git clone git@bitbucket.org:4amanuel/aman-ng-gemini.git .  ```
please veryify a ```docker``` and ```docker-compose``` installed on your system and assumes you are using Linux based system for ease
2)
run inside project folder
```
npm install

```

navigate to sub folder `cd firs-demo` and run 

```
npm install
``` 
build your docker container on the root project folder `cd ..`

```

docker-compose build 
```
3)
once the service is up run command on the serive (other terminal) 



To continue development >  creating new elements / developing please run  to solve file permission issues on your project directory.

```
sudo chown -R $USER:$USER . 

```



## Development server

Run `docker-compose up` for a dev server. Navigate to `http://localhost:3000/` http://localhost:3000.( please check the port is free or modify it on the `docker-compose.yml` to suit your needs.)

## To run any command on the container do : 
```
docker-compose exec ng-contain <your command here>

```
eg : to run the tests http://localhost:9876
```
docker-compose exec ng-contain ng test
```


## Code scaffolding

Run `docker-compose exec ng-contain ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


